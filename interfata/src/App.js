import React from 'react';
import './App.css';
import LoginDialog from './dialogs/LoginDialog';
import AdminPage from './dialogs/AdminPage';
import CopPage from './dialogs/CopPage'
import './style/butons.css'

function App (){

  return (
    <div>
      <LoginDialog/>
      <AdminPage />
      <CopPage />
    </div>
  );
  
}

export default App;
