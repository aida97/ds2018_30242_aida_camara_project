export const setUser = (user) =>{
    return {
        type: 'SET_USER',
        user
    }
}

export const showAdmin = (show) => {
    return {
        type: 'SHOW_ADMIN',
        show
    }
}

export const showClient = (show) => {
    return {
        type: 'SHOW_CLIENT',
        show
    }
}

export const showLogin = (show) => {
    return {
        type: 'SHOW_LOGIN',
        show
    }
}

export const setUsername = (username)=>{
    return {
        type: 'SET_USERNAME',
        username
    }
}

export function doLogin(username, password){
    return function(dispatch){
        const url = "http://localhost:8080/login";
        const data = {
            "username": username,
            "password": password
        }
        const fetchData = {
            method: 'POST',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json'}),
            body: JSON.stringify(data)
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            const {token, tip} = data;
            console.log(tip);
            if(tip === "ADMIN"){
                dispatch(showAdmin(true));
                dispatch(showClient(false));
                dispatch(showLogin(false));
                dispatch(setUser(token));
            }
            if(tip === "CLIENT"){
                dispatch(showAdmin(false));
                dispatch(showClient(true));
                dispatch(showLogin(false));
                dispatch(setUser(token));
                dispatch(setUsername(username))
            }
            

        }).catch(err=>console.log(err));
    }
}