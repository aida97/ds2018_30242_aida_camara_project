import {showResult, setResult} from './copsActions'
export function addEvidence(denumire, obs, idCaz, token){
    return function(dispatch) {
        const url = "http://localhost:8080/evidence";
        const data = {
            "denumire": denumire,
            "observatii": obs,
            "idCaz": idCaz
        }
        const fetchData = {
            method: 'POST',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            body: JSON.stringify(data)
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`A fost adaugata dovada: id: ${data.id} denumire: ${data.denumire}, observatii: ${data.observatii}, caz: ${data.caz.id}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function deleteEvidence(id, token){
    return function(dispatch) {
        const url = "http://localhost:8080/evidence/"+id;
        const fetchData = {
            method: 'DELETE',
            headers: new Headers({"Accept": 'application/json', "Token": token}),
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`ID ${id} sters`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function updateEvidence(denumire, obs, idCaz, token){
    return function(dispatch) {
        const url = "http://localhost:8080/evidence";
        const data = {
            "denumire": denumire,
            "observatii": obs,
            "idCaz": idCaz
        }
        const fetchData = {
            method: 'PUT',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            body: JSON.stringify(data)
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`${data.id}, ${data.denumire}, ${data.obs}, ${data.caz.id}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function getEvidence(id, token){
    return function(dispatch) {
        const url = "http://localhost:8080/evidence/"+id;
        
        const fetchData = {
            method: 'GET',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`ID: ${data.id}, denumire: ${data.denumire}, observati: ${data.observatii}, caz: ${data.caz.id}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}