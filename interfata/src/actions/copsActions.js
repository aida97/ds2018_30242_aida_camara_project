
export const toggleDialog = show=>{
    return{
        type: 'TOGGLE_DIALOG',
        show
    }
}

export const showResult=(show)=>{
    return {
        type: 'SHOW_RESULT',
        show
    }
}

export const setResult = (result)=>{
    return {
        type: 'SET_RESULT',
        result
    }
}

export const addNotif = (notif) => {
    return {
        type: 'ADD_NOTIF',
        notif
    }
}

export function addCop(username, password, token){
    return function(dispatch) {
        const url = "http://localhost:8080/users";
        const data = {
            "username": username,
            "password": password,
            "tip": "CLIENT"
        }
        const fetchData = {
            method: 'POST',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            body: JSON.stringify(data)
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`A fost adaugat clientul: id: ${data.id} username: ${data.username}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function deleteCop(id, token){
    return function(dispatch) {
        const url = "http://localhost:8080/users/"+id;
        const fetchData = {
            method: 'DELETE',
            headers: new Headers({"Accept": 'application/json', "Token": token}),
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`A fost sters clientul ${id}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function updateCop(id, idCaz, token){
    return function(dispatch) {
        const url = "http://localhost:8080/users/"+id+'/'+idCaz;
        
        const fetchData = {
            method: 'PUT',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`Politistul ${id} adaugat la cazul ${idCaz}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function getCop(id, token){
    return function(dispatch) {
        const url = "http://localhost:8080/users/"+id;
        
        const fetchData = {
            method: 'GET',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            
            dispatch(showResult(true))
            console.log(JSON.stringify(data))
            const {id, username, tip, cazuriList}=data;
            const list = cazuriList.map(function(e){
                return e.id;
            }).join(",")
            dispatch(setResult(`id: ${id}, username: ${username}, tip: ${tip}, cazuri: ${list}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}