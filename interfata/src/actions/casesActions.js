import {showResult, setResult} from './copsActions'

export const showRaportResult = (show)=>{
    return {
        type: 'SHOW_RAPORT_RESULT',
        show
    }
}

export const setRaportResult = (ani, sume)=>{
    return {
        type: 'SET_RAPORT_RESULT',
        ani,
        sume
    }
}
export function addCase(tip, emergency,situatie, token){
    return function(dispatch) {
        const url = "http://localhost:8080/cazuri";
        const data = {
            "tip": tip,
            "emergency": emergency,
            "situatie": situatie
        }
        const fetchData = {
            method: 'POST',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            body: JSON.stringify(data)
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`A fost adaugat cazul: id: ${data.id} tip: ${data.tip}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function deleteCase(id, token){
    return function(dispatch) {
        const url = "http://localhost:8080/cazuri/"+id;
        const fetchData = {
            method: 'DELETE',
            headers: new Headers({"Accept": 'application/json', "Token": token}),
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`ID ${id} sters`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function updateCase(id, situatie, token){
    return function(dispatch) {
        const url = "http://localhost:8080/cazuri/"+id;
        const data = {
            "situatie": situatie
        }
        const fetchData = {
            method: 'PUT',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            body: JSON.stringify(data)
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`Cazul cu id-ul ${data.id} are situatia ${data.situatie}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function getCase(id, token){
    return function(dispatch) {
        const url = "http://localhost:8080/cazuri/"+id;
        
        const fetchData = {
            method: 'GET',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(data=>{
            dispatch(showResult(true))
            dispatch(setResult(`ID: ${data.id}, Urgenta: ${data.emergency}, Tip: ${data.tip}, ${data.situatie}`))
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('Eroare'))
        });
    }
}

export function genRaport(anStart, anStop, token){
    return function (dispatch){
        const url = "http://localhost:8080/raport/"+anStart+"/"+anStop;
        
        const fetchData = {
            method: 'GET',
            headers: new Headers({"Content-Type": "application/json", "Accept": 'application/json', "Token": token}),
            
        }
        fetch(url, fetchData).then(resp=> resp.json()).then(rez=>{

            const {ani, data} = rez;
            dispatch(setRaportResult(ani, data))
            dispatch(showRaportResult(true))
            
        }).catch(err=>{
            dispatch(showResult(true))
            dispatch(setResult('eroare'))
        });
    }
    
}