import React from 'react'
import DropDownMenu from '../dialogs/DropDownMenu'
import AddCopDialog from './cops/AddCopDialog'
import DeleteCopDialog from './cops/DeleteCopDialog'
import SearchCopDialog from './cops/SearchCopDialog'
import AddCopToCaseDialog from './cops/AddCopToCaseDialog'
import AddCaseDialog from './cases/AddCaseDialog'
import DeleteCaseDialog from './cases/DeleteCaseDialog'
import SearchCaseDialog from './cases/SearchCaseDialog'
import UpdeateCaseStatus from './cases/UpdateCaseStatusDialog'
import ResultRaport from './ResultRaport'
import Result from './Result'
import { connect } from 'react-redux'
import Raport from './Raport'


function AdminPage(){
    
    return <div>
        <DropDownMenu text="Cops operations">
            <AddCopDialog/>
            <DeleteCopDialog/>
            <SearchCopDialog/>
            <AddCopToCaseDialog/>
        </DropDownMenu>

        <DropDownMenu text="Cases operations">
            <AddCaseDialog/>
            <DeleteCaseDialog/>
            <UpdeateCaseStatus/>
            <SearchCaseDialog/>
            <Raport/>
        </DropDownMenu>
        <Result/>
        <ResultRaport/>
        </div>
}

const mapStateToProps = (state) => {
    return {
        showPage: state.appReducer.get('showAdminPage')
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

function AdminPageWraper(props) {
    const {showPage} = props;
    return showPage && <AdminPage/>
} 

export default connect(mapStateToProps, mapDispatchToProps)(AdminPageWraper);