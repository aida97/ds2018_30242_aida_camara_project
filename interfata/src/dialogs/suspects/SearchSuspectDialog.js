import React, {useState} from 'react'
import useInput from '../useInuptHook'
import {store} from '../../index'
import { connect } from 'react-redux'
import {getEvidence} from '../../actions/suspectsActions'
import {toggleDialog} from '../../actions/copsActions'

function SearchSuspectDialog(props){
    const id = useInput();
    const [enabled, setEnabled]=useState(false);
    const {token} = props;

    const handleAdd = () =>{
        store.dispatch(getEvidence(id.value, token))
    }
    const handleCancel=()=>{
        setEnabled(false);
    }
    const handleClick = () =>{
        props.toggle(true);
        setEnabled(true);
    }
    const dialog = <div className="dialog">
    <p>Evidence Id: <input type="text" {...id}/></p>
    <p><button onClick={handleAdd}>Search evidence</button><button onClick={handleCancel}>Cancel</button></p>
</div>
    const button = <button className="searchSuspect" onClick={handleClick}>Search evidence</button>
    return enabled===true && props.showDialog===true? dialog:button;
}

const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}
export default connect(mapStateToProps, mapDispatchToProps )(SearchSuspectDialog);