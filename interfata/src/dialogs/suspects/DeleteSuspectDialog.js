import React, {useState} from 'react'
import useInput from '../useInuptHook'
import {store} from '../../index'
import { connect } from 'react-redux'
import {deleteEvidence} from '../../actions/suspectsActions'
import {toggleDialog} from '../../actions/copsActions'

function DeleteSuspectDialog(props){
    const id = useInput();
    const [enabled, setEnabled]=useState(false);
    const {token} = props;

    const handleAdd = () =>{
        store.dispatch(deleteEvidence(id.value, token))
    }
    const handleCancel=()=>{
        setEnabled(false);
    }
    const handleClick = () =>{
        props.toggle(true);
        setEnabled(true);
    }
    const dialog = <div className="dialog">
    <p>Evidence Id: <input type="text" {...id}/></p>
    <p><button onClick={handleAdd}>Delete evidence</button><button onClick={handleCancel}>Cancel</button></p>
</div>
    const button = <button className = "deleteSuspect"onClick={handleClick}>Delete evidence</button>
    return enabled===true && props.showDialog===true? dialog:button;
}

const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}
export default connect(mapStateToProps, mapDispatchToProps )(DeleteSuspectDialog);