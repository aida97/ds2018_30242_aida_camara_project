import React, {useState} from 'react';
import useInput from '../useInuptHook'
import { connect } from 'react-redux'
import {toggleDialog} from '../../actions/copsActions'
import {store} from '../../index'
import {addEvidence} from '../../actions/suspectsActions'

function AddSuspectDialog(props){
    const denumire = useInput();
    const obs = useInput();
    const idcaz = useInput();
    const [enabled, setEnabled]=useState(false);

    const {token}=props

    const handleAdd = () =>{
        store.dispatch(addEvidence(denumire.value,obs.value,idcaz.value,token))
    }
    const handleCancel=()=>{
        setEnabled(false);
    }
    const handleClick = () =>{
        props.toggle(true);
        setEnabled(true);
    }

    const dialog = <div className="dialog">
    <p>Denumire: <input type="text" {...denumire}/></p>
    <p>Observatii: <input type="text" {...obs}/></p>
    <p>Case ID: <input type="text" {...idcaz}/></p>
    <p><button onClick={handleAdd}>Add evidence</button><button onClick={handleCancel}>Cancel</button></p>
</div>
    const button = <button className="addSuspect" onClick={()=>handleClick()}>Add evidence</button>
    return enabled===true && props.showDialog===true? dialog:button;
}
const mapStateToProps = (state) => {
    return {
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }
}
const mapDispatchToProps = dispatch => {
    return {
        toggle: (show)=>dispatch(toggleDialog(show))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddSuspectDialog);