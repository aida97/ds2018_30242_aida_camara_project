import React , { useState } from 'react'



function DropDownMenu(props){

    const {text}=props;
    const [expanded, setExpanded]=useState(false);
    
    const handleClick = () => {
        setExpanded(!expanded);
    }

    const items = expanded && props.children;

    return <div>
        <button className="drpdwn" onClick={()=>handleClick()}>{text}</button>
        {items}
    </div>
}

export default DropDownMenu;