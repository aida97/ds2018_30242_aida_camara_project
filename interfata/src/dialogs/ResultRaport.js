import React from 'react'
import { connect } from 'react-redux'
import {showRaportResult} from '../actions/casesActions'
import {Bar} from 'react-chartjs-2';

function ResultRaport (props) {
    
    const {sume, ani}=props;

    let sume1 = [];
    let ani1 = [];

    if(sume._tail && ani._tail){
         sume1 = sume._tail.array;
         ani1 = ani._tail.array;
    
    }
    
    const data = {
        labels: ani1,
        datasets: [{
            label: 'Cazuri rezolvate',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: sume1,
        }]
    }
    return props.show && <div>
        <Bar data={data}/>
        <p><button onClick={()=>props.cancel(false)}>Close</button></p>
    </div>
}

const mapStateToProps = state => {
    return {
        show: state.appReducer.get('showRaportResult'),
        ani: state.appReducer.get('raportAni'),
        sume: state.appReducer.get('raportSume')
    }
}

const mapDispatchToProps = dispatch => {
    return {
        cancel: (show)=>dispatch(showRaportResult(show))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultRaport);
