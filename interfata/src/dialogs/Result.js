import React from 'react'
import { connect } from 'react-redux'
import {showResult} from '../actions/copsActions'
import PropTypes from 'prop-types';
import '../style/res.css'


function Result(props){

    const {result, show} = props;
    
    return show && <div class="balabal">
        <p>{result}</p>
        <p><button onClick={()=>props.cancel(false)}>Close</button></p>
    </div>
}

const mapStateToProps = state => {
    return {
        show: state.appReducer.get('showResult'),
        result: state.appReducer.get('setResult')
    }
}

const mapDispatchToProps = dispatch => {
    return {
        cancel: (show)=>dispatch(showResult(show))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Result);

Result.propTypes={
result: PropTypes.string
}