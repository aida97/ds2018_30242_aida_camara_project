import React, {useState}from 'react'
import useInput from '../useInuptHook'
import { connect } from 'react-redux'
import {toggleDialog} from '../../actions/copsActions'
import {store} from '../../index'
import {updateCase} from '../../actions/casesActions'

function UpdateCaseStatusDialog(props){

    const [enabled, setEnabled]=useState(false);
    const inputProps = useInput('NEW')
    const caseID = useInput()
    const {token} = props

    const handleAdd = () =>{
        store.dispatch(updateCase(caseID.value, inputProps.value, token))
    }
    const handleCancel = () => {
        setEnabled(false);
    }

    const handleClick = () =>{
        props.toggle(true);
        setEnabled(true);
    }

    const dialog = <div className="dialog">
    <p>Situatie: <select value={inputProps.value} onChange={inputProps.onChange}>
            <option value="NEW">Nou</option>
            <option value="IN_PROGRESS">Deschis</option>
            <option value="CLOSED">Inchis</option>
        </select></p>
    <p>Case ID: <input type="text" {...caseID}/></p>
    <p><button onClick={handleAdd}>Update case</button><button onClick={handleCancel}>Cancel</button></p>
</div>
    const button = <button className="updateCase" onClick={handleClick}>Update case status</button>
    return enabled===true && props.showDialog===true? dialog:button; 
}

const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateCaseStatusDialog);