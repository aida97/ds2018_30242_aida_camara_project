import React, {useState} from 'react'
import useInput from '../useInuptHook'
import { connect } from 'react-redux'
import {toggleDialog} from '../../actions/copsActions'
import {store} from '../../index'
import {addCase} from '../../actions/casesActions'

function AddCaseDialog(props){
    const [enabled, setEnabled]=useState(false);
    const tip = useInput('rapire')
    const situatie = useInput('NEW')
    const urgenta = useInput('mare')
    const {token}=props;

    const handleAdd = () => {
        store.dispatch(addCase(tip.value,urgenta.value,situatie.value,token))
    }

    const handleCancel = () => {
        setEnabled(false);
    }

    const handleClick = () =>{
        props.toggle(true);
        setEnabled(true);
    }
    const dialog = <div className="dialog">
    <p>Tip: <select value={tip.value} onChange={tip.onChange}>
            <option value="rapire">Rapire</option>
            <option value="arme">Arme</option>
            <option value="furt">Furt</option>
            <option value="liniste publica">Deranjarea linistii publice</option>
        </select></p>
    <p>Situatie: 
        <select value={situatie.value} onChange={situatie.onChange}>
            <option value="NEW">Nou</option>
            <option value="IN_PROGRESS">Deschis</option>
            <option value="CLOSED">Inchis</option>
        </select></p>
    <p>Urgenta: 
        <select value={urgenta.value} onChange={urgenta.onChange}>
            <option value="mare">Mare</option>
            <option value="medie">Medie</option>
            <option value="mica">Mica</option>
        </select>
    </p>
    <p><button onClick={()=>handleAdd()}>Add case</button><button onClick={handleCancel}>Cancel</button></p>
</div>;

    const button = <button className="addCaseBtn" onClick={handleClick}>Add case</button>;

    return enabled===true && props.showDialog===true? dialog:button
}

const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
        
    }
    
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCaseDialog);