import React, {useState} from 'react'
import useInput from '../useInuptHook'
import { connect } from 'react-redux'
import {toggleDialog} from '../../actions/copsActions'
import {store} from '../../index'
import {deleteCase} from '../../actions/casesActions'

function DeleteCaseDialog(props){
    const [enabled, setEnabled]=useState(false);
    const id = useInput()

    const {token} = props
    const handleAdd = () => {
        store.dispatch(deleteCase(id.value,token))
    }

    const handleCancel = () => {
        setEnabled(false);
    }

    const handleClick = () =>{
        props.toggle(true);
        setEnabled(true);
    }

    const dialog = <div className="dialog">
    <p>Case id: <input type="text" {...id}/></p>
    <p><button onClick={()=>handleAdd()}>Delete case</button><button onClick={()=>handleCancel()}>Cancel</button></p>
</div>;
    const button = <button className="deleteCaseBtn" onClick={handleClick}>Delete case</button>
    return enabled===true && props.showDialog===true? dialog:button
}
const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}
export default connect(mapStateToProps, mapDispatchToProps)(DeleteCaseDialog);