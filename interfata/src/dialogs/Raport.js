import React, {useState}  from 'react';
import { connect } from 'react-redux'
import {toggleDialog} from '../actions/copsActions'
import {store} from '../index'
import {genRaport} from '../actions/casesActions'
import useInput from './useInuptHook'

function Raport(props){
    const anStart = useInput();
    const anStop =  useInput();
    const [enabled, setEnabled]=useState(false);
    const {token}=props

    const handleAdd = () => {
        store.dispatch(genRaport(anStart.value, anStop.value, token))
    }

    const handleCancel = () => {
        setEnabled(false);
    }

    const handleClick= ()=>{
        props.toggle(true);
        setEnabled(true);
    }
    const dialog = <div className="dialog">
    <p>An start: <input type="text" {...anStart}/></p>
    <p>An stop: <input type="text" {...anStop}/></p>
    <p><button onClick={handleAdd}>Generate Raport</button><button onClick={handleCancel}>Cancel</button></p>
</div>;
    const button = <button className="raportBtn" onClick={handleClick}>Generate raport</button>
    return enabled===true && props.showDialog===true? dialog:button

}
const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Raport);