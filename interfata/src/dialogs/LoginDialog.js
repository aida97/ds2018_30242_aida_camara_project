import React , { useState } from 'react'
import {connect} from 'react-redux'
import {setUser, showAdmin, showClient, doLogin} from '../actions/loginActions'
import '../style/loginDialog.css'
import {store} from '../index'


function LoginDialog () {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = () => {
        store.dispatch(doLogin(username, password));
        
    }

    return (
        <div className = "loginDiv">
            <h1>Log in</h1>
            <p>Username: <input type="text" value={username} onChange={(e)=>setUsername(e.target.value)}/></p>
            <p>Password: <input type="text" value={password} onChange={(e)=> setPassword(e.target.value)}/></p>
            <p><button onClick={()=>handleLogin()}>Log in</button> </p>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        user: state.appReducer.get('user'),
        showPage: state.appReducer.get('showLogin')
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
       // doLogin: (username,password) => dispatch(doLogin(username, password)),
        setUser: (user) => dispatch(setUser(user)),
        showAdminPage: (show) => dispatch(showAdmin(show)),
        showClientPage: (show) => dispatch(showClient(show))
    }
}

function LoginWraper(props){
    return props.showPage && <LoginDialog/>
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginWraper);