import React, {useState, useEffect} from 'react'
import {connect }from 'react-redux'
import DropDownMenu from './DropDownMenu'
import AddSuspect from '../dialogs/suspects/AddSuspectDialog'
import DeleteSuspect from '../dialogs/suspects/DeleteSuspectDialog'
import SearchSuspect from '../dialogs/suspects/SearchSuspectDialog'
import Result from './Result'
import NotificationArea from '../dialogs/NotificationArea'
import {addNotif} from '../actions/copsActions'

function CopPage(props){

    const {setNotif} = props;
    let ws
    useEffect(()=>{
      ws = new WebSocket('ws://localhost:8080/name');
      ws.onmessage = function(data){
        setNotif(data.data);
      }
      ws.onopen = () => {
          ws.send(props.username)
      }
      console.log(ws.OPEN);
      console.log(ws.CLOSED)
      //while(ws.CONNECTING===0){}
      //if(ws.CONNECTING!==0){
        //ws.send(username);
      //}
      
      return ()=>{
        ws.close();
        console.log("Disconnected");
      }
    }, [props.username])

    
    

    return <div>
        <DropDownMenu text="Dovezi">
            <AddSuspect/>
            <DeleteSuspect/>
            <SearchSuspect/>
        </DropDownMenu>
        <Result/>
        <NotificationArea notif={props.notif}/>
    </div>
}

const mapStateToProps = (state) => {
    return {
        showPage: state.appReducer.get('showClientPage'),
        username: state.appReducer.get('username'),
        notif: state.appReducer.get('notif')
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setNotif: (not)=> dispatch(addNotif(not))
    }
}

function CopPageWraper (props){
    return props.showPage && <CopPage username={props.username} setNotif={props.setNotif} notif={props.notif}/>
}

export default connect(mapStateToProps, mapDispatchToProps)(CopPageWraper);