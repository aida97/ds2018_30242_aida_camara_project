import React ,{useState} from 'react'
import { connect } from 'react-redux'
import {toggleDialog} from '../actions/copsActions'

function Item (props){

    const {text, toggle}=props;
    const [selected, setSelected] = useState(false);

    const handleClick = ()=>{
        setSelected(!selected);
        toggle(true);
    }

    const dialog = props.children;

    return <div>
        <button onClick={()=>handleClick()}>{text}</button>
        {selected && dialog}
    </div>
}
const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}
export default connect(null,mapDispatchToProps)(Item);