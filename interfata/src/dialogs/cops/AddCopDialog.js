import React, {useState} from 'react'
import { connect } from 'react-redux'
import {toggleDialog} from '../../actions/copsActions'
import {store} from '../../index'
import {addCop} from '../../actions/copsActions'

function AddCopDialog(props){

    const [cnp, setCnp]=useState('');
    const [name, setName]=useState('');

    const [enabled, setEnabled]=useState(false);

    const {token}=props;

    const handleAdd = () => {
        store.dispatch(addCop(cnp, name, token));
    }

    const handleCancel = () => {
        setEnabled(false);
    }

    const handleClick = () =>{
        props.toggle(true);
        setEnabled(true);
    }
    const dialog = <div className="dialog">
        <p>Username: <input type="text" value={cnp} onChange={(e)=>{setCnp(e.target.value)}}/></p>
        <p>Password: <input type="text" value={name} onChange={(e)=>{setName(e.target.value)}}/></p>
        <p><button onClick={()=>handleAdd()}>Add cop</button><button onClick={handleCancel}>Cancel</button></p>
    </div>

    const button = <button className="addCopBtn" onClick={handleClick}>Add cop</button>

    return enabled===true && props.showDialog===true? dialog:button
}

const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}
export default connect(mapStateToProps, mapDispatchToProps)(AddCopDialog);