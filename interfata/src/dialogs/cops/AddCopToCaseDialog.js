import React, {useState}  from 'react';
import useInput from '../useInuptHook';
import { connect } from 'react-redux'
import {toggleDialog} from '../../actions/copsActions'
import {store} from '../../index'
import {updateCop} from '../../actions/copsActions'

function AddCopToCaseDialog(props){
    
    const copInputProps = useInput();
    const cazInput =  useInput();
    const [enabled, setEnabled]=useState(false);

    const {token} = props
    
    const handleAdd = () => {
        store.dispatch(updateCop(copInputProps.value, cazInput.value, token));
    }

    const handleCancel = () => {
        setEnabled(false);
    }

    const handleClick= ()=>{
        props.toggle(true);
        setEnabled(true);
    }
    const dialog = <div className="dialog">
    <p>Cop id: <input type="text" {...copInputProps}/></p>
    <p>Case id: <input type="text" {...cazInput}/></p>
    <p><button onClick={handleAdd}>Add cop to case</button><button onClick={handleCancel}>Cancel</button></p>
</div>;
    const button = <button className="updateCopBtn" onClick={handleClick}>Add cop to case</button>
    return enabled===true && props.showDialog===true? dialog:button

}
const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}

export default connect(mapStateToProps,mapDispatchToProps)(AddCopToCaseDialog);