import React, {useState}  from 'react';
import useInput from '../useInuptHook';
import { connect } from 'react-redux'
import {toggleDialog, deleteCop} from '../../actions/copsActions'
import {store} from '../../index'

function DeleteCopDialog(props){

    const inputProps = useInput();
    const [enabled, setEnabled]=useState(false);

    const {token}=props;

    const handleAdd = () => {
        store.dispatch(deleteCop(inputProps.value, token))
    }

    const handleCancel = () => {
        setEnabled(false);
    }

    const handleClick= ()=>{
        props.toggle(true);
        setEnabled(true);
    }
    const dialog = <div className="dialog">
    <p>Cop id: <input type="text" {...inputProps}/></p>
    <p><button onClick={()=>handleAdd()}>Delete cop</button><button onClick={handleCancel}>Cancel</button></p>
</div>;
    const button = <button className="deleteCopBtn" onClick={handleClick}>Delete cop</button>
    return enabled===true && props.showDialog===true? dialog:button

}
const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}

export default connect(mapStateToProps,mapDispatchToProps)(DeleteCopDialog);