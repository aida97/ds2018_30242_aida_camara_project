import React,{useState}  from 'react';
import useInput from '../useInuptHook';
import { connect } from 'react-redux';
import {toggleDialog, getCop} from '../../actions/copsActions'
import {store} from '../../index'


function SearchCopDialog(props){
    const inputProps = useInput();
    const [enabled, setEnabled] = useState(false);
    const {token}=props;
    const handleAdd = () => {
        store.dispatch(getCop(inputProps.value, token))
    }

    const handleCancel = () => {
        setEnabled(false);
    }
    const handleClick = ()=>{
        props.toggle(true);
        setEnabled(true);
    }
    const dialog = <div className="dialog">
    <p>Cop id: <input type="text" {...inputProps}/></p>
    <p><button onClick={handleAdd}>Search cop</button><button onClick={handleCancel}>Cancel</button></p>
</div>;

    const button = <button className="searchCopBtn" onClick={handleClick}>Search cop</button>
    return enabled===true && props.showDialog===true? dialog:button;

}

const mapStateToProps = state => {
    return{
        showDialog: state.appReducer.get('showDialog'),
        token: state.appReducer.get('user')
    }; 
}

const mapDispatchToProps = (dispatch) =>{
    return{
        toggle: (show)=>dispatch(toggleDialog(show))
    }
    
}

export default connect(mapStateToProps,mapDispatchToProps)(SearchCopDialog);