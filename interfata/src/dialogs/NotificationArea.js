import React from 'react'


function NotificationArea (props){
    
    const {notif} = props; 
    
    const style = {
      "whiteSpace":"pre-line"
    }

  return <div className="notifDiv">
    <p>Notificarile tale sunt aici!</p>
    <span style={style}>{notif}</span>
    
  </div>
}

export default NotificationArea;