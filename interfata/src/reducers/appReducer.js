import {fromJS} from 'immutable';
const initialState = fromJS({
    showDialog: false,
    showAdminPage: false,
    showClientPage: false,
    user: "NOTLOGGEDIN",
    showLogin: true,
    showResult: false,
    showRaportResult: false,
    setResult: '',
    raportAni: [],
    raportSume: [],
    username: '',
    notif: ''

});
export const appReducer = (state = initialState, action) => {

    let newState
    switch (action.type){
        case 'TOGGLE_DIALOG':
            newState = state.set('showDialog', fromJS(action.show))
            return newState;

        case 'SET_USER':
            newState = state.set('user', fromJS(action.user))
            return newState;

        case 'SHOW_ADMIN':
            newState = state.set('showAdminPage', fromJS(action.show))
            return newState;

        case 'SHOW_CLIENT':
            newState = state.set('showClientPage', fromJS(action.show))
            return newState;

        case 'SHOW_LOGIN':
            newState = state.set('showLogin', fromJS(action.show))
            return newState;

        case 'SHOW_RESULT':
            newState = state.set('showResult', fromJS(action.show))
            return newState;

        case 'SET_RESULT':
            newState = state.set('setResult', fromJS(action.result))
            return newState;

        case 'SHOW_RAPORT_RESULT':
            newState = state.set('showRaportResult', fromJS(action.show))
            return newState;

        case 'SET_RAPORT_RESULT':
            newState = state.set('raportAni', fromJS(action.ani));
            newState = newState.set('raportSume', fromJS(action.sume));
            return newState;
        
        case 'SET_USERNAME':
            newState = state.set('username', fromJS(action.username));
            return newState;

        case 'ADD_NOTIF':
            const old = state.get('notif');
            const nn = old.concat('\n ').concat(action.notif);
            newState = state.set('notif', fromJS(nn));
            return newState;

        default:
            return state;
    }
}