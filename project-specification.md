# Criminal Investigation Tracker

# Descrierea proiectului

## Introducere

In orice comunitate civilizata exista macar o autoritate care a impus reguli de conduita pentru a asigura bunul trai in colectiv. Totusi, intotdeauna exista oameni care incalca aceste legi; politia are obligativitatea de a ii gasi si sanctiona pentru abaterea de la normele impuse. In prezent, putem folosi tehnologia pentru a face procesul de investigare a unei crime mai usor si mai rapid.  

Aplicatia dezvoltata in acest proiect este destinata departamentului de politie, oferind o modalitate de a centraliza toate datele disponibile despre cazurile aflate in curs de rezolvare sau despre cele finalizate. Aplicatia ofera o cale usoara de a vizualiza evolutia unui caz pentru a ii ajuta pe oamenii legii sa poata aiba o vedere de ansamblu. 

Publicul care va folosi aceasta aplicatie va avea nevoie de cunostinte minime de utlizarea a unui computer, deoarece interfata grafica este foarte intuitiva. 

## Scop

Scopul aplicatiei este de a eficientiza activitatea departamentului de politie. Utilizarea aplicatiei va duce la minimizarea timpului alocat pentru rezolvarea unei crime. Se va crea o baza de date in care se vor gasi toate crimele care au avut loc. Centralizarea datelor cu posibilitatea de a interoga in diverse moduri baza de date, ii va ajuta pe utilizatori sa faca conexiuni logice mai bune intre diverse evenimente.

Un alt scop al aplicatiei este de a renunta la utilizarea hartiei prin digitalizarea datelor. In prezent, in Romania un dosar penal al unei persoane nu poate fi furnizat in format digital, ceea ce este o foarte mare problema atat in ceea ce priveste spatiul, deteriorarea dar si defrisarea padurilor.

## Domeniu

Autoritatile care poate fi ajutate de aceasta aplicatie includ statul roman, ministerul de justitie, SRI si departamentele de politie din tara.

## Definitii, Acronime si Abrevieri

Politie: Organ de stat însărcinat cu menținerea ordinii publice și cu reprimarea infracțiunilor

Politist: Functioar de politie

Suspect: Persoana care este banuita de fapte ilegale

Justitie: Totalitatea organelor de jurisdicție dintr-un stat; ansamblul legilor și al instanțelor judecătorești; sistemul de funcționare a acestor instanțe.

SRI: Sistemul roman de informatii

O bază de date, uneori numită și bancă de date (abreviat BD), reprezintă o modalitate de stocare a unor informații și date pe un suport extern (un dispozitiv de stocare), cu posibilitatea extinderii ușoare și a regăsirii rapide a acestora.

O interogare a bazei de date (Query) este o cerere către baza de date de a obţine date ce satisfac anumite criterii. Operaţia de interogare produce o tabelă cu datele cerute, care se va numi în continuare tabelă rezultat.


## Vedere de ansamblu
In sectiunile urmatoare din acest document se pot gasi mai multe detalii despre proiectul implementat.

Sectiunea **_Pozitionare_** indica unde se incadreaza produsul din punctul de vedere al caracteristicilor sale.

In subsectiunea **_Definirea problemei_** este descris motivul pentru care este nevoie de aceasta aplicatie, cine are nevoie de ea si cum isi propune sa rezolve problema prezentata.

In subsectiuna **_Definirea pozitiei produsului_** este prezentata audienta tinta si diferentele fata de sistemele deja existente.

Sectiunea **_Parti interesate si utilizatori_** contine o descriere detaliata a celor care ar putea fi interesati in achizitionarea unei astfel de aplicatii si persoanele care o vor utiliza si problemele cheie pe care ei le intalnesc si care sunt rezolvate de prolema propusa.

Cerintele minime (hardware/software) pentru a putea utiliza aplicatia sunt mentionate in sectiunea **_Cerintele produsului_**.

# Pozitionare
## Definirea problemei

|||
|----|------- |
| **Problema** | necentralizarii tuturor datelor
| **afecteaza**  | departamentele de politie ale Romaniei
| **avand impactul** |  imposibilitatea de a vizualiza usor informatii despre cazuri
| **o solutie ar fi** | implementarea unor baze de date in care sa se faseasca toate evenimentele, persoanele implicate si dovezilor gasite
| **care ar duce la** | minimizarea timpului alocat rezolvarii unui caz


## Definirea pozitiei produsului


|||
|----|------- |
| **Pentru** | Departamentele de politie din Roamnia |
| **Care** | nu au o digitalizare a informatiilor |
| **The** | Criminal Investigation Tracker with Suspect Prediction este o aplicatie
| **Care** | faciliteaza cautarea usoara a diverselor date si care ofera o prezicere a unui rezultat care ar putea incheia un caz
| **Fata de** | neutilizarea tehnologiei 
| **Produsul meu** | utlizeaza baze de date si algoritmi 

# Parti interesate si utilizatori

## Descrierea partilor interesate

### Partea interesata
* **Nume**: Ministerul justitiei
* **Descriere**: 
* **Responsabilitati**: 
    * asigura ca aplicatia va fi introdusa in sistem
    * asigura ca aplicatia va fi utilizata conform manualului
    * asigura pregatirea utilizatorilor printr-un curs dedicat
    * numeste un administrator al aplicatiei
    * ofera fonduri pentru dezvoltarea ulterioara 
    

## Descrierea utilizatorilor

### Utilizator 1
* **Nume**: Administrator
* **Descriere**: administreaza aplicatia
* **Responsabilitati**: 
    * CRUD politisti
    * CRUD cazuri
    * genereaza un raport care va avea informatii despre performanta anuala a sistemului de politie


### Utilizator 2
* **Nume**: Agent de politie
* **Descriere**: foloseste aplicatia pentru a participa la rezolvarea cazurilor
* **Responsabilitati**:
    * CRUD suspecti
    * isi verifica intotdeauna notificarile legate de cazurile de care se ocupa

## Mediul Utilizator

La un caz pot fi asignati oricati politisti, aplicatia nu are astfel de constrangeri. Pot fi introduse oricat de multe cazuri, suspecti si dovezi. Capacitatea bazei de date fiind de 32TB intr-un tabel. 

Aplicatia nu poate fi folosita pe telefoane inteligente, necesita conexiune la internet si se poate folosi pe orice sistem de operare.

Produsul utilizeza o server pentru crearea bazelor de date, de aceea este nevoie ca acestea sa fie active pentru ca produsul oferit sa poata fi folosit. 


# Cerinte non-functionale


## Fiabilitatea (Availabiltiy)

Fiabilitatea se poate defini ca timpul (momentele din zi si zilele din an) in care aplicatia este disponibila/nu este disponibia pentru utilizare. Sistemul trebuie sa fie functionabil 24 de ore pe zi, sapte zile pe saptamana.
Aceasta cerinta este responsabilitatea administratorului, care trebuie sa se asigure ca serverul este pornit si functional. Pornirea serverului face posibila conectarea utlizatorilor la aplicatie. 
O cadere a serverului nu are trebui sa se intample mai des de o data la 300 de ore.

## Performanta (Performance)

Performanta sistemului este reprezentata ca o descriere a 4 caracateristici ale aplicatiei: timpul de raspuns, volumul de lucru, scalabilitatea si consideratiile platformei.

### Timpul de raspuns

Tinem cont de urmatoarii timpi:
* 0.1 s: raspuns instant
* 1 s: limita maxima pentru ca utilizatorul sa nu simta o intrerupere in activitate
* 10 s: limita maxima pentru ca utilizatorul sa nu renunte la activitatea pe care o desfasura

Perioada de masurare este definitaca timpul trecut intre momentul in care utilizatorul a realizat o cerere catre aplicatie si momentul in care aplicatia afiseaza un raspuns.

Aplicatia indeplineste aceasta cerinta astfel:

Timpul pentru logare: 0.2 s

Timpul pentru selectarea unei otiuni din meniu: 0.1 s

Timpul pentru interogarea bazei de date: 0.1 s

Timpul pentru actualizarea bazei de date: 0.3 s

### Volum de lucru (Workload)

In aceasta subsectiune sunt descrise scenariile pe care utilizatorii sunt cel mai probabil sa le execute.

Scenariu | Total zilnic | Pagini | Timp de gandire
---------|--------------|--------|----------------
Adauga politist| 10| Log in, Introduce politst, Exit| 30 s
Adauga caz| 30| Log in, Introduce caz, Introduce politsti la caz, Exit| 120 s
Adauga suspect| 150| Log in, Adauga susupect, Exit| 30 s
Vizualizare prezicere| 30| Log in, Vizualizare prezicere, Selecteaza caz, Exit| 30 s

### Scalabilitatea (Scalabilty)

Scalabilitatea aplicatiei este descrisa drept cresterea maxima a volumului de lucru pe care sistemul o poate procesa.
Testarea scalabilitatii aplicatiei a fost facuta utlizand sisteme hardware aditionale pentru a simula o crestere mare a utilizatorilor. In urma testelor, se poate afirma ca cerintele in ceea ce priveste timpul de raspuns sunt inca indeplinite odata cu cresterea volumului de lucru. 

### Consideratiile platformei

Platforma este hardware-ul sau software-ul (sistemul de operare si alte utilitati software implicate) care gazduiesc sistemul. 
Aplicatia trebuie sa poata fi utilizata pe orice sistem de operare care dispune de ultima versiune de JRE. 

## Securitate (Security)

Sistemul nu este vulnerabil din punctul de vedere al securitatii. Astfel, unui atacator nu ii va fi permis sa preia privilegii in  user's system. Sistemul de autentificare va fi bazat pe un token.

# Uzabilitate (Usability)

Definirea cerintelor de uzabilitate implica trei activitati inrudite: analizarea contextului de utilizare, definirea secenariilor care pot fi testate si specificarea cerintelor pentru eficacitate, eficienta si satisfactie.

### Contextul utilizarii

Aplicatia are doua tipuri de utilizatori: Administrator si agent de politie.

* **Administratorul** are urmatoarele responsabilitati: 
      * CRUD politisti
      *  CRUD cazuri
      * genereaza un raport care va avea informatii despre performanta anuala a sistemului de politie

* **Agentul de politie** are urmatoarele responsbilitati:
    *  CRUD suspecti
    *  isi verifica intotdeauna notificarile legate de cazurile de care se ocupa
enariile de utilizare

# Scenariile de utilizare

## Scenariu 1
* **Scenariu **: Generare raport
* **Nivel**:  user-goal level
* **Actor principal**: Administrator
* **Main success scenario**: 
  * Utilizatorul se logheaza ca administrator, 
  * introduce corect user-name-ul si parola, 
  * selecteaza din meniu optiunea de generare raport, 
  * introduce perioada de timp pe care sa se genereze raportul, 
  * deschide fiserul generat de catre sistem 
* **Extensions**: Datele de logare sunt incorecte.

## Scenariu  2
* **Scenariu **: Verifica notificari
* **Level**: user-goal level
* **Actor principal**: Politist
* **Main success scenario**: 
  * Utilizatorul se logheaza ca politist, 
  * introduce corect user-name-ul si parola, 
  * observa in meniul de notificari o noua notificare,
  * selecteaza notificarea,
  * verifica cazul care a fost actualizat
* **Extensions**: Datele de logare sunt incorecte. 


## Diagrama de relatii intre entitati

[er](project ER.png)

