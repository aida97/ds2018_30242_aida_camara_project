package ds.project.demo.controllers;

import com.sun.org.apache.xpath.internal.operations.Bool;
import ds.project.demo.auth.AuthUtils;
import ds.project.demo.entities.User;
import ds.project.demo.requestEntities.UserRequest;
import ds.project.demo.servicies.UserService;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.POST, maxAge = 1800L)
    @PostMapping(path="/users", consumes = "application/json", produces = "application/json")
    public ResponseEntity<User> saveUser(@RequestHeader(value="Token") String jwt, @RequestBody UserRequest userRequest){
        try {
            if(AuthUtils.validateAdminToken(jwt) && AuthUtils.validateExpiration(jwt)){
                return new ResponseEntity<>(userService.saveUser(userRequest), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.PUT, maxAge = 3600L)
    @PutMapping(path="/users/{userId}/{cazId}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<User> updateUser(@RequestHeader(value="Token") String jwt,@PathVariable int userId, @PathVariable int cazId){
        try {
            if (AuthUtils.validateAdminToken(jwt)) {
                return new ResponseEntity<>(userService.updateUser(userId, cazId), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.GET, maxAge = 3600L)
    @GetMapping(path="/users/{userId}")
    public ResponseEntity<User> getUser(@RequestHeader(value="Token") String jwt,@PathVariable int userId){

        try {
            if (AuthUtils.validateAdminToken(jwt) && AuthUtils.validateExpiration(jwt)) {
                return new ResponseEntity<>(userService.getUser(userId), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.DELETE, maxAge = 3600L)
    @DeleteMapping("/users/{userId}")
    public ResponseEntity<Boolean> deleteUser(@RequestHeader(value="Token") String jwt, @PathVariable int userId){
        try {
            if (AuthUtils.validateAdminToken(jwt) && AuthUtils.validateExpiration(jwt)) {
                userService.deleteUser(userId);
                return new ResponseEntity<>(true, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
