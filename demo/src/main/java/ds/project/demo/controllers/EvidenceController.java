package ds.project.demo.controllers;

import ds.project.demo.auth.AuthUtils;
import ds.project.demo.entities.Evidence;
import ds.project.demo.mappers.Mapper;
import ds.project.demo.requestEntities.EvidenceRequest;
import ds.project.demo.servicies.EvidenceService;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class EvidenceController {

    private EvidenceService evidenceService;


    @Autowired
    public EvidenceController(EvidenceService evidenceService) {
        this.evidenceService = evidenceService;
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.POST, maxAge = 1800L)
    @PostMapping("/evidence")
    public ResponseEntity<Evidence> saveEvidence(@RequestHeader(value="Token") String jwt, @RequestBody EvidenceRequest evidenceRequest){
        try {
            if(AuthUtils.validateClientToken(jwt) && AuthUtils.validateExpiration(jwt))
            return new ResponseEntity<Evidence>(evidenceService.saveEvidence(evidenceRequest), HttpStatus.OK);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.PUT, maxAge = 3600L)
    @PutMapping("/evidence/{evidenceId}")
    public ResponseEntity<Evidence> updateEvidence(@RequestHeader(value="Token") String jwt,@PathVariable int evidenceId, @RequestBody EvidenceRequest evidenceRequest){
        try {
            if(AuthUtils.validateClientToken(jwt) && AuthUtils.validateExpiration(jwt))
            return new ResponseEntity<>(evidenceService.updateEvidence(evidenceId, evidenceRequest), HttpStatus.OK);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.GET, maxAge = 3600L)
    @GetMapping("/evidence/{id}")
    public ResponseEntity<Evidence> getEvidence(@RequestHeader(value="Token") String jwt,@PathVariable int id){
        try {
            if(AuthUtils.validateClientToken(jwt) && AuthUtils.validateExpiration(jwt))
            return new ResponseEntity<>(evidenceService.getEvidence(id), HttpStatus.OK);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.DELETE, maxAge = 3600L)
    @DeleteMapping("/evidence/{id}")
    public ResponseEntity<Boolean> deleteEvidence(@RequestHeader(value="Token") String jwt,@PathVariable int id){
        try {
            if(AuthUtils.validateClientToken(jwt) && AuthUtils.validateExpiration(jwt)){
                evidenceService.deleteEvidence(id);
                return new ResponseEntity<>(true, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
