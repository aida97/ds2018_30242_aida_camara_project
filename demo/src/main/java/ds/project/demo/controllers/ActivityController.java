package ds.project.demo.controllers;

import ds.project.demo.auth.AuthUtils;
import ds.project.demo.requestEntities.ActivityResponse;
import ds.project.demo.servicies.ActivityService;
import io.jsonwebtoken.JwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityController {

    private ActivityService activityService;

    public ActivityController(ActivityService activityService) {
        this.activityService = activityService;
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.GET, maxAge = 3600L)
    @GetMapping("/raport/{anStart}/{anStop}")
    public ResponseEntity<ActivityResponse> generateRaport(@RequestHeader(value="Token") String jwt, @PathVariable int anStart, @PathVariable int anStop){
        try {
            if(AuthUtils.validateAdminToken(jwt) && AuthUtils.validateExpiration(jwt)){
                return new ResponseEntity<>(activityService.generateRaport(anStart, anStop), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
