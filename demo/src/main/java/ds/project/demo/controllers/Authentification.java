package ds.project.demo.controllers;


import ds.project.demo.auth.AuthUtils;
import ds.project.demo.entities.User;
import ds.project.demo.requestEntities.LoginRequest;
import ds.project.demo.requestEntities.LoginResponse;
import ds.project.demo.servicies.UserService;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.http.HTTPException;
import java.security.Key;
import java.util.Date;

@RestController
public class Authentification {


    private final long EXPIRATION = 5 * 3600*1000;
    private UserService userService;

    @Autowired
    public Authentification(UserService userService) {
        this.userService = userService;
    }

    //private final String[] headers = {"Content-Type", "Accept"};

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept"}, methods = RequestMethod.POST, maxAge = 3600L)
    @PostMapping(path="/login", consumes = "application/json", produces = "application/json")
    public LoginResponse login(@RequestBody LoginRequest loginRequest){
        User user = userService.findUser(loginRequest.getUsername(), loginRequest.getPassword());
        if(user!=null){
            String token = AuthUtils.createJWT(user.getId(),user.getUsername(), user.getTip(), EXPIRATION);
            LoginResponse loginResponse = new LoginResponse(token, user.getTip());
            return loginResponse;
        }
        return null;
    }


}
