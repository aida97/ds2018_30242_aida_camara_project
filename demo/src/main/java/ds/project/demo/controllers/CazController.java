package ds.project.demo.controllers;

import ds.project.demo.auth.AuthUtils;
import ds.project.demo.entities.Caz;
import ds.project.demo.requestEntities.CazRequest;
import ds.project.demo.servicies.CazService;
import io.jsonwebtoken.JwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CazController {

    private CazService cazService;

    public CazController(CazService cazService) {
        this.cazService = cazService;
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.POST, maxAge = 1800L)
    @PostMapping("/cazuri")
    public ResponseEntity<Caz> saveCaz(@RequestHeader(value="Token") String jwt,@RequestBody CazRequest cazRequest){
        try {
            if(AuthUtils.validateAdminToken(jwt) && AuthUtils.validateExpiration(jwt)){
                return new ResponseEntity<>(cazService.saveCaz(cazRequest), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.PUT, maxAge = 3600L)
    @PutMapping("/cazuri/{cazId}")
    public ResponseEntity<Caz> updateCaz(@RequestHeader(value="Token") String jwt, @PathVariable int cazId,@RequestBody String situatie){
        try {
            if(AuthUtils.validateAdminToken(jwt) && AuthUtils.validateExpiration(jwt)){
                return new ResponseEntity<>(cazService.updateCaz(cazId, situatie), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.GET, maxAge = 3600L)
    @GetMapping("/cazuri/{cazId}")
    public ResponseEntity<Caz> getCaz(@RequestHeader(value="Token") String jwt,@PathVariable int cazId){
        try {
            if(AuthUtils.validateAdminToken(jwt) && AuthUtils.validateExpiration(jwt)){
                return new ResponseEntity<>(cazService.getCaz(cazId), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @CrossOrigin(origins="http://localhost:3000", allowedHeaders={"Content-Type", "Accept", "Token"}, methods = RequestMethod.DELETE, maxAge = 3600L)
    @DeleteMapping("/cazuri/{cazId}")
    public ResponseEntity<Boolean> deleteCaz(@RequestHeader(value="Token") String jwt,@PathVariable int cazId){
        try {
            if (AuthUtils.validateAdminToken(jwt) && AuthUtils.validateExpiration(jwt)) {
                cazService.deleteCaz(cazId);
                return new ResponseEntity<>(true, HttpStatus.OK);
            } else {
                return new ResponseEntity<>( HttpStatus.UNAUTHORIZED);
            }
        } catch (JwtException e) {
            return new ResponseEntity<>( HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
