package ds.project.demo.mappers;


import ds.project.demo.entities.Caz;
import ds.project.demo.entities.Evidence;
import ds.project.demo.entities.User;
import ds.project.demo.requestEntities.CazRequest;
import ds.project.demo.requestEntities.EvidenceRequest;
import ds.project.demo.requestEntities.UserRequest;

public class Mapper {

    public User map(UserRequest userRequest){
        return new User(userRequest.getUsername(), userRequest.getPassword(), userRequest.getTip());
    }

    public Caz map(CazRequest cazRequest){
        return new Caz(cazRequest.getTip(), cazRequest.getEmergency(), cazRequest.getSituatie());
    }

    public Evidence map(EvidenceRequest evidenceRequest) {
        return new Evidence(evidenceRequest.getDenumire(),evidenceRequest.getObservatii());
    }
}
