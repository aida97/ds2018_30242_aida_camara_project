package ds.project.demo.servicies;

import ds.project.demo.entities.Activity;
import ds.project.demo.entities.Caz;
import ds.project.demo.mappers.Mapper;
import ds.project.demo.repositories.ActivityRepository;
import ds.project.demo.repositories.CazRepository;
import ds.project.demo.requestEntities.CazRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;

import static java.time.Year.now;


@Service
public class CazService {

    private CazRepository cazRepository;
    private ActivityRepository activityRepository;
    private Mapper mapper = new Mapper();

    @Autowired
    public CazService(CazRepository cazRepository, ActivityRepository activityRepository) {
        this.cazRepository = cazRepository;
        this.activityRepository = activityRepository;
    }

    public Caz saveCaz(CazRequest cazRequest) {
        return cazRepository.save(mapper.map(cazRequest));
    }

    public Caz updateCaz(int cazId, String cazRequest) {
        Caz caz = cazRepository.findById(cazId).orElseThrow(()->new HTTPException(404));
        cazRequest=cazRequest.substring(13, cazRequest.length()-2);
        if(!caz.getSituatie().equals("CLOSED") && cazRequest.equals("CLOSED")){
            Activity activity = new Activity(cazId, now().toString());
            activityRepository.save(activity);
        }
        caz.setSituatie(cazRequest);
        return cazRepository.save(caz);
    }

    public Caz getCaz(int cazId) {

        return cazRepository.findById(cazId).orElseThrow(()->new HTTPException(404));
    }

    public void deleteCaz(int cazId) {
        Caz caz = cazRepository.findById(cazId).orElseThrow(()->new HTTPException(404));
        cazRepository.delete(caz);
    }
}
