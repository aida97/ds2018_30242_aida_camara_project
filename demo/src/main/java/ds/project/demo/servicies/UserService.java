package ds.project.demo.servicies;

import ds.project.demo.entities.Caz;
import ds.project.demo.entities.User;
import ds.project.demo.mappers.Mapper;
import ds.project.demo.repositories.CazRepository;
import ds.project.demo.repositories.UsersRepository;
import ds.project.demo.requestEntities.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;

@Service
public class UserService {

    private UsersRepository usersRepository;
    private CazRepository cazRepository;
    private Mapper mapper = new Mapper();
    private NotificationService notificationService;

    @Autowired
    public UserService(UsersRepository usersRepository, CazRepository cazRepo, NotificationService notificationService) {
        this.usersRepository = usersRepository;
        this.cazRepository = cazRepo;
        this.notificationService = notificationService;
    }

    public User saveUser(UserRequest userRequest) {
        return usersRepository.save(mapper.map(userRequest));
    }

    public User updateUser(int userId, int cazId) {
        User user = usersRepository.findById(userId).orElseThrow(()->new HTTPException(404));
        Caz caz = cazRepository.findById(cazId).orElseThrow(()->new HTTPException(404));
        user.addCase(caz);
        notificationService.addCaz(user.getUsername(), caz);
        return usersRepository.save(user);
    }

    public User getUser(int userId) {
        return usersRepository.findById(userId).orElseThrow(()->new HTTPException(404));
    }

    public void deleteUser(int userId) {
        User user = usersRepository.findById(userId).orElseThrow(()->new HTTPException(404));
        usersRepository.delete(user);
    }

    public User findUser(String username, String password){
        return usersRepository.findUserByUsernameAndPassword(username,password);
    }
}
