package ds.project.demo.servicies;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import ds.project.demo.entities.Caz;
import ds.project.demo.entities.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@Component
public class NotificationService implements WebSocketHandler {

    //private List<WebSocketSession> sessions = new ArrayList<>();
    private Map<WebSocketSession, String> sessionMap = new HashMap<>();
   // private Map<String, Caz> cazMap = new HashMap<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        //sessions.add(webSocketSession);

    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {

        sessionMap.put(webSocketSession, webSocketMessage.getPayload().toString());
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {

    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
       // sessions.remove(webSocketSession);
        sessionMap.remove(webSocketSession);
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    public void addCaz(String user, Caz caz){
       // cazMap.put(user,caz);
    }

    public void sendNotifications(Caz caz, Set<String> users){
        //Set<String> users = cazMap.entrySet().stream().filter(e->e.getValue().equals(caz)).map(Map.Entry::getKey).collect(Collectors.toSet());
        Set<WebSocketSession>sessions= sessionMap.entrySet().stream().filter(e->users.contains(e.getValue())).map(Map.Entry::getKey).collect(Collectors.toSet());

        for(WebSocketSession session: sessions){
            WebSocketMessage message = new TextMessage("Notificare despre cazul "+caz.getId());
            try {
                session.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /*private static final String EXCHANGE_NAME = "topic_logs";

    public void sendNotification(String routingKey, String message){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.exchangeDeclare(EXCHANGE_NAME, "topic");

            channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + routingKey + "':'" + message + "'");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }*/



}
