package ds.project.demo.servicies;

import ds.project.demo.repositories.ActivityRepository;
import ds.project.demo.requestEntities.ActivityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityService {

    private ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public ActivityResponse generateRaport(int anStart, int anStop) {
        ActivityResponse activityResponse = new ActivityResponse();
        List<Integer> ani = activityResponse.getAni();
        List<Integer> data = activityResponse.getData();
        for(int i=anStart; i<=anStop; i++){
            ani.add(i);
            data.add(activityRepository.countActivitiesByTime(Integer.toString(i)));
        }
        return activityResponse;
    }
}
