package ds.project.demo.servicies;

import ds.project.demo.entities.Caz;
import ds.project.demo.entities.Evidence;
import ds.project.demo.entities.User;
import ds.project.demo.mappers.Mapper;
import ds.project.demo.repositories.CazRepository;
import ds.project.demo.repositories.EvidenceRepository;
import ds.project.demo.requestEntities.EvidenceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EvidenceService {

    private EvidenceRepository evidenceRepository;
    private CazRepository cazRepository;
    private Mapper mapper = new Mapper();
    private NotificationService notificationService;

    @Autowired
    public EvidenceService(EvidenceRepository evidenceRepository, CazRepository cazRepository, NotificationService notificationService) {
        this.evidenceRepository = evidenceRepository;
        this.cazRepository = cazRepository;
        this.notificationService = notificationService;
    }

    public Evidence saveEvidence(EvidenceRequest evidenceRequest) {
        Caz caz = cazRepository.findById(evidenceRequest.getIdcaz()).orElseThrow(()->new HTTPException(404));
        Evidence evidence = mapper.map(evidenceRequest);
        evidence.setCaz(caz);
        Evidence newEvidence = evidenceRepository.save(evidence);
        Set<User> users = caz.getUserList();
        Set<String> usernames = users.stream().map(User::getUsername).collect(Collectors.toSet());
        notificationService.sendNotifications(caz, usernames);
        return newEvidence;
    }

    public Evidence updateEvidence(int evidenceId, EvidenceRequest evidenceRequest) {
        Evidence evidence = evidenceRepository.findById(evidenceId).orElseThrow(()->new HTTPException(404));
        evidence.setDenumire(evidenceRequest.getDenumire());
        evidence.setObservatii(evidenceRequest.getObservatii());
        Caz caz = cazRepository.findById(evidenceRequest.getIdcaz()).orElseThrow(()->new HTTPException(404));
        evidence.setCaz(caz);
        return evidenceRepository.save(evidence);
    }

    public Evidence getEvidence(int id) {
        return evidenceRepository.findById(id).orElseThrow(()->new HTTPException(404));
    }

    public void deleteEvidence(int id) {
        Evidence evidence = evidenceRepository.findById(id).orElseThrow(()->new HTTPException(404));
        evidenceRepository.delete(evidence);
    }
}
