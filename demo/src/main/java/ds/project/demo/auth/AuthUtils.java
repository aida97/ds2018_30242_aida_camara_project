package ds.project.demo.auth;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

public class AuthUtils {
    private static final String SECRET_KEY="B9BA38AC732CACB7FAA8A9DC4A9B27F9CE19822865533BAD6FF7A5B969";

    public static String createJWT(int id, String username, String tip, long expiration) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        JwtBuilder builder = Jwts.builder().setId(Integer.toString(id))
                .setIssuedAt(now)
                .setSubject(tip)
                .setIssuer(username)
                .signWith(signatureAlgorithm, signingKey);

        if (expiration > 0) {
            long expMillis = nowMillis + expiration;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        return builder.compact();

    }

    public static Claims decodeJWT(String jwt) {
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }

    public static boolean validateAdminToken(String jwt){
        Claims claims = AuthUtils.decodeJWT(jwt);
        return claims.getSubject().equals("ADMIN");
    }

    public static boolean validateClientToken(String jwt){
        Claims claims = AuthUtils.decodeJWT(jwt);
        return claims.getSubject().equals("CLIENT");
    }

    public static boolean validateExpiration(String jwt){
        Claims claims = AuthUtils.decodeJWT(jwt);
        return claims.getExpiration().toInstant().toEpochMilli()-System.currentTimeMillis()>0;
    }
}
