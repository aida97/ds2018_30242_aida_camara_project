package ds.project.demo.entities;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="dovezi")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Evidence {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="denumire")
    private String denumire;

    @Column(name="observatii")
    private String observatii;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="idcaz", nullable=false)
    @JsonManagedReference
    private Caz caz;

    public Evidence() {
    }

    public Evidence(String denumire, String observatii) {
        this.denumire = denumire;
        this.observatii = observatii;
    }

    public Evidence(String denumire, String observatii, Caz caz) {
        this.denumire = denumire;
        this.observatii = observatii;
        this.caz = caz;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public String getObservatii() {
        return observatii;
    }

    public void setObservatii(String observatii) {
        this.observatii = observatii;
    }

    public Caz getCaz() {
        return caz;
    }

    public void setCaz(Caz caz) {
        this.caz = caz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Evidence evidence = (Evidence) o;
        return id == evidence.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
