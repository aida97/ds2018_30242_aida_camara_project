package ds.project.demo.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="cazuri")
public class Caz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="tip")
    private String tip;

    @Column(name="urgenta")
    private String emergency;

    @Column(name="situatie")
    private String situatie;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "caz")
    @JsonBackReference
    private Set<Evidence> evidenceList = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "cazuriList")
    @JsonBackReference
    private Set<User> userList = new HashSet<>();

    public Caz() {
    }

    public Caz(String tip, String emergency, String situatie) {
        this.tip = tip;
        this.emergency = emergency;
        this.situatie = situatie;
        this.evidenceList = new HashSet<>();
        this.userList = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getEmergency() {
        return emergency;
    }

    public void setEmergency(String emergency) {
        this.emergency = emergency;
    }

    public String getSituatie() {
        return situatie;
    }

    public void setSituatie(String situatie) {
        this.situatie = situatie;
    }

    public Set<Evidence> getEvidenceList() {
        return evidenceList;
    }

    public void setEvidenceList(Set<Evidence> evidenceList) {
        this.evidenceList = evidenceList;
    }

    public Set<User> getUserList() {
        return userList;
    }

    public void setUserList(Set<User> userList) {
        this.userList = userList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Caz caz = (Caz) o;
        return id == caz.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
