package ds.project.demo.entities;


import javax.persistence.*;

@Entity
@Table(name="activities")
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="idcaz")
    private int idCaz;

    @Column(name="time")
    private String time;

    public Activity() {
    }

    public Activity(int idCaz, String time) {
        this.idCaz = idCaz;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCaz() {
        return idCaz;
    }

    public void setIdCaz(int idCaz) {
        this.idCaz = idCaz;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
