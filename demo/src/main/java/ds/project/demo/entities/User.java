package ds.project.demo.entities;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table( name = "users")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(name="username")
    private String username;


    @Column(name="password")
    private String password;


    @Column(name="tip")
    private String tip;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    @JoinTable(
            name = "users_cazuri",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "caz_id") }
    )
    @JsonManagedReference
    private Set<Caz> cazuriList = new HashSet<>();

    public User() {
    }

    public User(String username, String password, String tip) {
        this.username = username;
        this.password = password;
        this.tip = tip;
        this.cazuriList = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Set<Caz> getCazuriList() {
        return cazuriList;
    }

    public void setCazuriList(Set<Caz> cazuriList) {
        this.cazuriList = cazuriList;
    }

    public void addCase(Caz caz){
        cazuriList.add(caz);
        caz.getUserList().add(this);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (username != null ? !username.equals(user.username) : user.username != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (tip != null ? !tip.equals(user.tip) : user.tip != null) return false;
        return cazuriList != null ? cazuriList.equals(user.cazuriList) : user.cazuriList == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (tip != null ? tip.hashCode() : 0);
        result = 31 * result + (cazuriList != null ? cazuriList.hashCode() : 0);
        return result;
    }
}
