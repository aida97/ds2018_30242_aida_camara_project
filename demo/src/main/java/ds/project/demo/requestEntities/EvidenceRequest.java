package ds.project.demo.requestEntities;


import ds.project.demo.entities.Caz;

public class EvidenceRequest {
    private String denumire;
    private String observatii;
    private int idCaz;

    public EvidenceRequest(String denumire, String observatii, int idcaz) {
        this.denumire = denumire;
        this.observatii = observatii;
        this.idCaz = idcaz;
    }

    public EvidenceRequest(){}

    public String getDenumire() {
        return denumire;
    }

    public String getObservatii() {
        return observatii;
    }

    public int getIdcaz() {
        return idCaz;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public void setObservatii(String observatii) {
        this.observatii = observatii;
    }

    public void setIdCaz(int idCaz) {
        this.idCaz = idCaz;
    }
}
