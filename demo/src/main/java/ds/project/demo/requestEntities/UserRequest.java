package ds.project.demo.requestEntities;


public class UserRequest {
    private String username;
    private String password;
    private String tip;

    public UserRequest() {
    }

    public UserRequest(String username, String password, String tip) {
        this.username = username;
        this.password = password;
        this.tip = tip;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTip() {
        return tip;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }
}
