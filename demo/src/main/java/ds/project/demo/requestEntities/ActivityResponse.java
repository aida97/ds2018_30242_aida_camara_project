package ds.project.demo.requestEntities;


import java.util.ArrayList;
import java.util.List;

public class ActivityResponse {
    private List<Integer> ani = new ArrayList<>();
    private List<Integer> data = new ArrayList<>();

    public ActivityResponse() {
    }

    public ActivityResponse(List<Integer> ani, List<Integer> data) {
        this.ani = ani;
        this.data = data;
    }

    public List<Integer> getAni() {
        return ani;
    }

    public void setAni(List<Integer> ani) {
        this.ani = ani;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }
}
