package ds.project.demo.requestEntities;




public class CazRequest {
    private String tip;
    private String emergency;
    private String situatie;


    public CazRequest(String tip, String emergency, String situatie) {
        this.tip = tip;
        this.emergency = emergency;
        this.situatie = situatie;
    }

    public CazRequest(){}
    public String getTip() {
        return tip;
    }

    public String getEmergency() {
        return emergency;
    }

    public String getSituatie() {
        return situatie;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public void setEmergency(String emergency) {
        this.emergency = emergency;
    }

    public void setSituatie(String situatie) {
        this.situatie = situatie;
    }
}
