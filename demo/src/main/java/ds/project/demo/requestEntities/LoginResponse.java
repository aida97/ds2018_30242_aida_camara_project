package ds.project.demo.requestEntities;


public class LoginResponse {

    private String token;
    private String tip;

    public LoginResponse(String token, String tip) {
        this.token = token;
        this.tip = tip;
    }

    public LoginResponse() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }
}
