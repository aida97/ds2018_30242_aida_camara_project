package ds.project.demo.repositories;

import ds.project.demo.entities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {

    Integer countActivitiesByTime(String an);
}
