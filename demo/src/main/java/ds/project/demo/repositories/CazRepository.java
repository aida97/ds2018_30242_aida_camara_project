package ds.project.demo.repositories;

import ds.project.demo.entities.Caz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CazRepository extends JpaRepository<Caz, Integer> {
}
